# Project 4: Brevet time calculator with Ajax

This project is a reimplementation of the RUSA ACP controle time calculator, however, timestamps for control points are updated automatically upon entry. The calculator allows for a brevet distance to be set, and provides timestamps for each control point entered. This implementation will provide the exact same output as the calculator at: https://rusa.org/octime_acp.html

### Author: Alec Springel, aspring6@uoregon.edu ##
---
## Getting Started:
First, build the docker image with:  
```docker build -t brevet .```  
To run the program run:  
```docker run -d -p 5000:5000 brevet``` and connect via a web browser  

---
## Using the Calculator
To use the calculator, set a starting date and time, as well as a brevet distance. Begin entering control distances in either miles or kilometers. Control times will be automatically populated. **It is important to note that if you change the starting date, time, or brevet distance, all entries will be cleared.**  
  
After you have entered each control distance, press the green "Submit" button below the table. A list of all control timestamps will be entered into the database. Clicking the submit button will cause the control distances to be replaced with whatever timestamps are in the table at that moment. 
  
To view the timestamps in the database, press the "Display" button.
### **Invalid Input:**
* Pressing the "Submit" button with zero timestamps in the table will result in an "alert". Nothing will be sent to the database.
* Entering a value 20% greater than the brevet distance is invalid. Your input will be cleared, and you will be alerted of the error
* Entering a value less than 0 (negative entries), or non-numbers is invalid. You will be alerted if this happens.
* As mentioned previously, changing the starting date, time, or brevet distance will clear all entries in order to prevent mistakes in control calculations.

---
## How it Works & Notes for Developers
### **Time Stamp Calculation**
All time stamps are rounded to the nearest minute (if seconds > 0.5 -> round up). Time stamps are calculated using the following table:  
| Control Location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr) |
|-----------------------|:---------------------:|:---------------------:|
| 0 -200                |           15          |           34          |
| 200 - 400             |           15          |           32          |
| 400 - 600             |           15          |           30          |
| 600 - 1000            |         11.428        |           28          |
| 1000 - 1300           |         13.333        |           26          |
However, it is important to note that if a brevet falls into one of these ranges, it is not only calculated with the speeds in each row, but also the speeds in previous (lower) rows. For instance,  
```
Calculating a control at 300km:
Close time = (100 * 15) + (200 * 15)
Open time = (100 * 32) + (200 * 34)

*The 100 comes from subtracting the next interval in the row from the control distance (300 - 200 = 100)

Calculating a control at 500km:
Close time = (100 * 15) + (200 * 15) + (200 * 15)
Open time = (100 * 30) + (200 * 32) + (200 * 34)
```
There are some instances where this calculation does not apply.
### **Oddities**
* A control located at 0 will have an open time equal to the start date/time, and a close 1 hour later
* If the control is located within the first 60km, the closing time is calculated by adding 1 hour to the start time, and then conducting the calculation using a minimum speed of of 20 km/hr

### **Handling Errors**
* NaN entries, or entries that are greater than 20% of the brevet distance return a -1 value from the backend. A -1 value in open_time or close_time results in an alert() in the front end which notifies the user.
---
## Communication With MongoDB
### **Sending Timestamps to the Database**
Upon pressing the "Submit" button, all timestamps are sent to the MongoDB, assuming that there is at least one valid timestamp in the table.  
  
A POST request is sent with the lists of the open and close times as a single string, with dates separated by commas. For instance,  
```
If the table contains:
open:  Sun 1/1 0:17     Mon 1/2 1:15
close: Sun 1/1 1:29     Mon 1/2 9:20

The POST request is sent as:
open=,Sun 1/1 0:17,Mon 1/2 1:15
close=,Sun 1/1 1:29,Mon 1/2 9:20
```
The flask backend then processes the arguments by parsing the string by each comma. Related open and close times will be at matching indexes, as it is impossible to have an open time without a corresponding close time.
### **Displaying the MongoDB Contents**
Upon hitting "Display", a GET request is sent to the MongoDB which returns a list of date/time documents, each containing:
```
date = {
    _id: The ID of the object from the database
    open: Open time of the control
    close: Close time of the control
}
```
This information is then displayed in the same window below the table. This is done by appending formatted div's containing each document into the "#db-result" div.
### **Handling Errors**
* Invalid control points/timestamps are handled by the backend
* Anytime the submit button is clicked with valid timestamps, the timestamps in the table overwrite thos in the MongoDB
* Anytime the display button is clicked, the last valid submission is the one displayed
* If the submit button is clicked without any timestamps in the table, the user is alerted and nothing is communicated to MongoDB
* If the submit button is clicked multiple times (without changing controls), the POST with the timestamps is still sent. This is because we want to allow the use to add additional timestamps if necessary, and submit/display them as needed. (Keep in mind, the displaying is done in the same window, so no redirect is occurring)
